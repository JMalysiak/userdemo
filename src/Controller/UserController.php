<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Jan Małysiak <jan.a.malysiak@gmail.com>
 */
class UserController
{
    /**
     * @Route("/user")
     */
    public function index(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $users = $entityManager->getRepository(User::class)
            ->findAll();

        $json = $serializer->serialize($users, 'json');

        return new Response($json);
    }
}
