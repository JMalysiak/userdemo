<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class UserFixtures
 *
 * @author Jan Małysiak <jan.a.malysiak@gmail.com>
 */
class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1
            ->setUserName('admin')
            ->setUserEmail('admin@example.org')
        ;

        $user2 = new User();
        $user2
            ->setUserName('gienek')
            ->setUserEmail('gienek@example.org')
            ->setUserPhone('123456789')
        ;

        $user3 = new User();
        $user3
            ->setUserName('stefek')
            ->setUserEmail('stefek@example.org')
            ->setUserPhone('987654321')
        ;

        $user4 = new User();
        $user4
            ->setUserName('zdzichu')
            ->setUserEmail('zdzislaw@example.org')
            ->setUserPhone('123123123')
        ;

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->persist($user4);

        $manager->flush();
    }
}
