<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="idUser")
     */
    private $idUser;

    /**
     * @ORM\Column(type="string", length=50, unique=true, name="userName")
     */
    private $userName;

    /**
     * @ORM\Column(type="string", length=50, unique=true, name="userEmail")
     */
    private $userEmail;

    /**
     * @ORM\Column(type="string", length=20, nullable=true, name="userPhone")
     */
    private $userPhone;

    public function getIdUser()
    {
        return $this->idUser;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    public function setUserName(string $userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function setUserEmail(string $userEmail): self
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    public function getUserPhone(): ?string
    {
        return $this->userPhone;
    }

    public function setUserPhone(?string $userPhone): self
    {
        $this->userPhone = $userPhone;

        return $this;
    }
}
