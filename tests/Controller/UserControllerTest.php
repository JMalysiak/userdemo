<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserControllerTest
 *
 * @author Jan Małysiak <jan.a.malysiak@gmail.com>
 */
class UserControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/user');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $users = json_decode($content, true);

        $this->assertNotEmpty($users);
        $this->assertArrayHasKey('idUser', $users[0]);
        $this->assertArrayHasKey('userName', $users[0]);
    }
}
